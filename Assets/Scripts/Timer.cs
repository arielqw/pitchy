﻿using System.Collections;
using System.Collections.Generic;
using System;
using TMPro;
using UnityEngine;

public class Timer : MonoBehaviour {

    public TextMeshPro timerText;

    private bool isTimerRunning;

    private DateTime timerStartTime;

    public KeywordInvoker keywordInvoker;

	// Use this for initialization
	void Start () {
        keywordInvoker.OnStartedLecture += StartTimer;
    }
	
	// Update is called once per frame
	void Update () {
		if (isTimerRunning)
        {
            var currentTimerTimespan = DateTime.Now - timerStartTime;
            var seconds = currentTimerTimespan.Seconds < 10 ? "0" + currentTimerTimespan.Seconds.ToString() : currentTimerTimespan.Seconds.ToString();
            var minutes = currentTimerTimespan.Minutes < 10 ? "0" + currentTimerTimespan.Minutes.ToString() : currentTimerTimespan.Minutes.ToString();
            timerText.text = string.Format("{0}:{1}", minutes, seconds);
        }
	}

    private void StartTimer()
    {
        isTimerRunning = true;
        timerStartTime = DateTime.Now;
    }

    private void StopTimer()
    {
        isTimerRunning = false;
    }
}
