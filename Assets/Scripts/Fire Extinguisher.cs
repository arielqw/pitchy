﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class FireExtinguisher : MonoBehaviour {

    public GameObject ParticleSystem;

    private void OnTriggerStay(Collider collider)
    {
        VRTK_InteractUse usingController = (collider.gameObject.GetComponent<VRTK_InteractUse>() ? collider.gameObject.GetComponent<VRTK_InteractUse>() : collider.gameObject.GetComponentInParent<VRTK_InteractUse>());
        ParticleSystem.gameObject.SetActive(CanUse(usingController));
    }

    private bool CanUse(VRTK_InteractUse usingController)
    {
        return (usingController && usingController.GetUsingObject() == null && usingController.IsUseButtonPressed());
    }
}
