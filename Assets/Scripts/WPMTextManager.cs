﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WPMTextManager : MonoBehaviour {

    public TextMeshPro wpmText;
    public KeywordInvoker keywordInvoker;

    private bool isLecturing;

	// Use this for initialization
	void Start () {
        keywordInvoker.OnStartedLecture += HandleStartedLecture;	
	}

    private void HandleStartedLecture()
    {
        isLecturing = true;
    }

    private void HandleLecturePaused()
    {
        isLecturing = false;
    }

    // Update is called once per frame
    void Update () {
		if (isLecturing)
        {
            wpmText.text = "WPM: " + keywordInvoker.GetWPM().ToString();
        }
	}
}
