﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using VRTK;
using System;

public class SlidesHandler : MonoBehaviour {

    public VRTK_ControllerEvents rightControllerEvents;
    public VRTK_ControllerEvents leftControllerEvents;
    public SpriteRenderer smallScreenSpriteRenderer;
    public SpriteRenderer bigScreenSpriteRenderer;
    private List<Sprite> m_presentationSprites = new List<Sprite>();
    private int currentSlideIndex;

    void Start()
    {
        var imagePaths = getImageFileNames();
        createPresentationSprites(imagePaths);
        ShowSlide();
        rightControllerEvents.TriggerPressed += HandleControllerClicked;
        leftControllerEvents.TriggerPressed += HandleControllerClicked;
        rightControllerEvents.TouchpadPressed += HandleGripClicked;
        leftControllerEvents.TouchpadPressed += HandleGripClicked;
    }

    private void HandleGripClicked(object sender, ControllerInteractionEventArgs e)
    {
        ShowPreviousSlide();
    }

    private void HandleControllerClicked(object sender, ControllerInteractionEventArgs e)
    {
        ShowNextSlide();
    }

    public void ShowNextSlide()
    {
        currentSlideIndex++;
        ShowSlide();
    }

    public void ShowPreviousSlide()
    {
        currentSlideIndex--;
        ShowSlide();
    }

    private void ShowSlide()
    {
        currentSlideIndex = Mathf.Clamp(currentSlideIndex, 0, m_presentationSprites.Count - 1);
        smallScreenSpriteRenderer.sprite = m_presentationSprites[currentSlideIndex];
        bigScreenSpriteRenderer.sprite = m_presentationSprites[currentSlideIndex];
    }

    #region LoadPresentationLogic

    private const string c_imageLocations = "C:\\Users\\Garage-Admin\\Documents\\PitchyVR\\Assets\\Slide Images\\";

    private List<string> getImageFileNames()
    {
        var imagePaths = new List<string>();
        var info = new DirectoryInfo(c_imageLocations);
        var fileInfo = info.GetFiles();
        foreach (var file in fileInfo)
        {
            if (file.Name.Contains(".meta"))
            {
                continue;
            } else
            {
                imagePaths.Add(file.Name);
            }
        }

        return imagePaths;
    }

    private void createPresentationSprites(List<string> imageFileNames)
    {
        foreach (var imageFileName in imageFileNames)
        {
            var texture = loadTextureFromFile(c_imageLocations + imageFileName);
            var sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f));
            m_presentationSprites.Add(sprite);
        }
    }

    private Texture2D loadTextureFromFile(string filePath)
    {
        byte[] fileData;
        var texture = new Texture2D(2, 2);

        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            ImageConversion.LoadImage(texture, fileData);
        }

        return texture;
    }

#endregion
}
