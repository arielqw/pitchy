﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class SafetyGoggles : MonoBehaviour
{
    public VRTK_HeadsetCollision m_headsetCollision;
    public GameObject m_cameraTint;

	// Use this for initialization
	void Start ()
	{
	    m_headsetCollision = FindObjectOfType<VRTK_HeadsetCollision>();
        m_headsetCollision.HeadsetCollisionDetect += MHeadsetCollisionOnHeadsetCollisionDetect;
        Debug.Log("Found: " + m_headsetCollision);
	}

    private void MHeadsetCollisionOnHeadsetCollisionDetect(object sender, HeadsetCollisionEventArgs headsetCollisionEventArgs)
    {
        var collider = headsetCollisionEventArgs.collider;
        if (collider != null && collider.tag == "Safety Goggles")
        {
            Destroy(collider.gameObject);
            m_cameraTint.gameObject.SetActive(true);
        }
    }

}
