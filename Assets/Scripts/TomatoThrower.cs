﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TomatoThrower : MonoBehaviour {

    public List<Transform> startingPositions;
    public Transform throwAtPoint;
    public GameObject tomatoPrefab;
    public AudioSource audioSource;
    public AudioClip booSound;

    private int index;
    public float throwForceMultiplier = 20f;

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.H))
        {
            StartThrowingTomatoes();
        }
	}

    public void StartThrowingTomatoes()
    {
        audioSource.Stop();
        audioSource.volume = 1;
        audioSource.clip = booSound;
        audioSource.Play();
        InvokeRepeating("ThrowTomato", 1.5f, 0.2f);
    }

    public void ThrowTomato()
    {
        if (index > startingPositions.Count - 1)
        {
            index = 0;
        }
        var startingPosition = startingPositions[index].position;
        var tomato = Instantiate(tomatoPrefab, startingPosition, Quaternion.identity);
        Debug.Log("INSTNAINTINITATE");
        var throwDirection = (throwAtPoint.position - startingPosition).normalized;
        tomato.GetComponent<Rigidbody>().AddForce(throwDirection * throwForceMultiplier);
        index++;
    }

}
