﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Linq;
using UnityEngine.Windows.Speech;
using System;

public class DictationRecognizerManager : MonoBehaviour
{

    DictationRecognizer dictation;
    bool isDictating;
    double dictTime; //dictation time in seconds

    double totalDictTime =0; // total dictation time IN MINIUTES 
    int totalWordCount = 0;  // total words recognized

    private int currentWPM = 0;

    ArrayList sentences;

    // Use this for initialization
    void Start()
    {
        sentences = new ArrayList();

        dictation = new DictationRecognizer();
        dictation.DictationResult += DictationResult;
        dictation.DictationHypothesis += DictationHypothesis;
        dictation.DictationError += DictationError;
        dictation.DictationComplete += DictationComplete;

        //beginDictation();
        //Debug.Log(PhraseRecognitionSystem.isSupported);


    }

    public void beginDictation()
    {
        isDictating = false; //reset all vars
        dictTime = 0;
        totalDictTime = 0;
        totalWordCount = 0;

        dictation.Start();
        Debug.Log("Dictation Started");
    }

    public void endDictation()
    {
        dictation.Stop();
        Debug.Log("Dictation Ended");
    }

    // Update is called once per frame
    void Update()
    {
         if(isDictating)
        {
            dictTime += Time.deltaTime;
        }
    }

    public double getCurrentWPM()
    {
        return currentWPM;
    }

    public double getAverageWPM()
    {
        return totalWordCount / totalDictTime;
    }

    public ArrayList getSentences()
    {
        return sentences;
    }


    private void DictationResult(string text, ConfidenceLevel confidence)
    {
        Debug.Log("Result;");
        Debug.Log(text + " " + confidence.ToString());
        Debug.Log("Time Elapsed: " + dictTime);

        //dictTime -= .5; //offset for dictation lag

        int wordCount = text.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Count();
        Debug.Log("WPM of this sentence: " + (wordCount / (dictTime/60)));
        currentWPM = (int)(wordCount / (dictTime / 60));
        totalWordCount += wordCount;
        totalDictTime += (dictTime / 60);
        isDictating = false;
        dictTime = 0;

        sentences.Add(text);

        String build = "";

        foreach (String sent in sentences)
        {
            build += sent + "; ";
            Debug.Log(sent);
        }

        Debug.Log("Total WPM so far: " + getCurrentWPM());
        Debug.Log("Text So Far: \n" + build);
    }

    private void DictationHypothesis(string text)
    {
        isDictating = true;
        Debug.Log("Hypothesis:");
        Debug.Log(text);
    }

    private void DictationError(string err, int hres)
    {
        Debug.Log("Dictation Error");
        Debug.Log(err + " " + hres);
    }

    private void DictationComplete(DictationCompletionCause cause)
    {
        /*if(cause.CompareTo(DictationCompletionCause.TimeoutExceeded) == 0)
        {
            Debug.Log("Timeout. Restarting Engine");
            //dictation.Stop();      //see if this line is nessessary later
            dictation.Start();

            
            return;
        }*/
        Debug.Log("Dictation Complete");
        Debug.Log(cause.ToString());
    }
}