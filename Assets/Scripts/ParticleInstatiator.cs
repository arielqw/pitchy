﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class ParticleInstatiator : MonoBehaviour
{

    public GameObject Particle;
    public float SpawnDelay;

    private float _spawnDelayTimer = 0f;

    private void OnTriggerStay(Collider collider)
    {
        VRTK_InteractGrab grabbingController = (collider.gameObject.GetComponent<VRTK_InteractGrab>() ? collider.gameObject.GetComponent<VRTK_InteractGrab>() : collider.gameObject.GetComponentInParent<VRTK_InteractGrab>());
        if (CanGrab(grabbingController) && Time.time >= _spawnDelayTimer)
        {
            var particle = Instantiate(Particle) as GameObject;
            grabbingController.GetComponent<VRTK_InteractTouch>().ForceTouch(particle);
            grabbingController.AttemptGrab();
            _spawnDelayTimer = Time.time + SpawnDelay;
        }
    }

    private bool CanGrab(VRTK_InteractGrab grabbingController)
    {
        return (grabbingController && grabbingController.GetGrabbedObject() == null && grabbingController.IsGrabButtonPressed());
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
