﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Linq;
using UnityEngine.Windows.Speech;
using System;


public class KeywordInvoker : MonoBehaviour {

    KeywordRecognizer keywordRecognizer;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();

    GameObject dictation;

    public Action OnStartedLecture;

    private DictationRecognizerManager dictationRecognizerManager;

    void Start () {

        Action dbstart = (() =>
        {
            Debug.Log("Activated!!");
        });

        Action dbstop = (() =>
        {
            Debug.Log("Do a stop");
        });

        Action startDictation = (() =>
        {
            StartDictation();
        });

        //Words and actions
        keywords.Add("activate", dbstart);
        keywords.Add("start", startDictation);
        keywords.Add("stop", dbstop);
        keywords.Add("begin", startDictation);

        keywordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());
        keywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
        keywordRecognizer.Start();

        //setup, but not start, dictation
        dictation = new GameObject();
        dictation.AddComponent<DictationRecognizerManager>();
    }
	
    private void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        OnStartedLecture.Invoke();
        Action keywordAction;
        // if the keyword recognized is in our dictionary, call that Action.
        if (keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }

    private void StartDictation()
    {
        PhraseRecognitionSystem.Shutdown();
        dictationRecognizerManager = dictation.GetComponent<DictationRecognizerManager>();
        dictationRecognizerManager.beginDictation();
    }

    public int GetWPM()
    {
        return (int)dictationRecognizerManager.getCurrentWPM();
    }

    public void RestartRecognizer()
    {
        if(!keywordRecognizer.IsRunning)
        {
            keywordRecognizer.Start();
        }
    }
}
